import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VueYoutube from 'vue-youtube';
import VueSocketIO from 'vue-socket.io';
import { io } from 'socket.io-client';
import VueI18n from 'vue-i18n';

Vue.config.productionTip = false
Vue.use(VueYoutube);
Vue.use(new VueSocketIO({
    debug: ( process.env.NODE_ENV !== 'production' ),
    connection: io(),
    vuex: {
        store,
        actionPrefix: 'socket_',
        mutationPrefix: 'socket_'
    },
}))
Vue.use(VueI18n);

const messages = {
    "en-US": require("./i18n/en-US.json"),
    "zh-HK": require("./i18n/zh-HK.json")
};
const fallbackLocale = Object.keys(messages)[0];
const locale = localStorage.getItem('locale') || fallbackLocale;
const i18n = new VueI18n({ locale, fallbackLocale, messages });

export const app = new Vue({
    store,
    vuetify,
    i18n,
    render: h => h(App),
}).$mount("#app");
