import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      playlist: [],
      jump_in_line: [],
      room_id: null,
      socket_connected: false,
      room_connected: false,
      playing: false,
      role: 'room',
      socket_id: null,
      rtc_enabled: true, 
      rtc_track_count: 0,
      msg_board_enabled: true,
  },
  mutations: {
      add_song_by_id: function(state, payload){
          if( state.playlist.find( (item) => item.id == payload.id ) ){ return ;}
          if( state.jump_in_line.find( (item) => item.id == payload.id ) ){ return ;}
          state.playlist.push(payload);
      },
      skip_current_song: function(state){
          state.playlist.shift();
          if(state.jump_in_line.length>0){
              state.playlist.unshift(state.jump_in_line[0]);
              state.jump_in_line.shift();
          }
      },
      assign_room_id: function(state, payload){
          state.room_id=payload;
          state.room_connected=true;
      },
      update_socket_connection_state: function(state, payload){
          state.socket_connected=payload;
      },
      update_room_connection_state: function(state, payload){
          state.room_connected=payload;
      },
      switch_play_pause: function(state){
          state.playing=!state.playing;
      },
      update_rtc_track_count: function(state, payload){
          state.rtc_track_count=payload;
      },
      update_socket_id: function(state, payload){
          state.socket_id=payload;
      },
      shuffle_playlist: function(state){
          if(state.playlist.length>1){
              var tmp = state.playlist.slice(1);
              tmp.sort( () => Math.random() - 0.5 );
              tmp.unshift(state.playlist[0]);
              state.playlist=tmp;
          }
      },
      song_jump_in_line: function(state, payload){
          var idx;
          if( ( idx=state.jump_in_line.findIndex( (item) => item.id == payload ) ) >=0 ){
              const song = state.jump_in_line[idx];
              state.jump_in_line.splice(idx, 1);
              state.jump_in_line.unshift(song);
          }else if( ( idx=state.playlist.findIndex( (item) => item.id == payload ) ) >0 ){
              const song = state.playlist[idx];
              state.playlist.splice(idx, 1);
              state.jump_in_line.push(song);
          }
      },
      switch_rtc: function(state){
          state.rtc_enabled=!state.rtc_enabled;
      },
      switch_msg_board: function(state){
          state.msg_board_enabled=!state.msg_board_enabled;
      }
  },
  actions: {
      socket_remote_add_song_by_id: function(context, payload){
          context.commit('add_song_by_id', payload);
      },
      socket_remote_skip_current_song: function(context){
          context.commit('skip_current_song');
      },
      socket_connect: function(context){
          context.commit('update_socket_connection_state', true);
      },
      socket_disconnect: function(context){
          context.commit('update_socket_connection_state', false);
          context.commit('update_room_connection_state', false);
      },
      socket_remote_switch_play_pause: function(context){
          context.commit('switch_play_pause');
      },
      socket_remote_shuffle_playlist: function(context){
          context.commit('shuffle_playlist');
      },
      socket_remote_song_jump_in_line: function(context, payload){
          context.commit('song_jump_in_line', payload);
      }
  },
  modules: {
  }
})
