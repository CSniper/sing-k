module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    devServer: {
        proxy: {
            '/socket.io': {
                target: 'http://localhost:3000/',
                changeOrigin: true,
                ws: true,
            },
            '/youtube': {
                target: "http://localhost:3000/",
                changeOrigin: true,
            }
        }
    },
    chainWebpack: (config) => {
        config.resolve.symlinks(false)
        config.plugin('html').tap( args => {
            args[0].title="唱K - Sing K";
            return args;
        })
    },
    configureWebpack: {
        devtool: 'source-map'
    }
}
