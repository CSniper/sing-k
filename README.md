# Sing K

This is a node.js project to make an online karaoke room using express + socket io + WebRTC + YouTube. I started this project not just because it's been a whole year of lockdown and social distancing, but that I think there are places where the typical karaoke experience can be improved. For instance, normally we can only select commerical published songs, we cannot select indi music and many amazing covers; or maybe we have a big group of friends together but not enough mics are provide. In this project I tried to use the YouTube and WebRTC API to make a karaoke with better functionality.

This demo is host on herokuapp -> url: [https://sing-k-app.herokuapp.com/](https://sing-k-app.herokuapp.com/)  
Enjoy!!

## YouTube Embedded Player + YouTube Data API (Worked)
This part handles the videos and search. Keywords or YouTube ID is sent to a REST api in the nodejs backend, then the actual query using YouTube Data API is executed and the result is processed and send back to front-end. The room maintained a playlist and play using the YouTube Embedded iframe player. 

## Room <=> Remote interaction (Worked)
This part is mainly done by socket io. When a room is initiated it creats a socket room too for the remote to connect and control. Playlist update, play control(eg play/pause, shuffle, skip), WebRTC signaling channel, drawing board all rely on the socket io channel.

## redis for caching and scalability (Worked)
Redis services is used for 3 purpose, caching youtube data api result, storing room states, and the communication layer for multi node socketio formation. 

## WebRTC (latency issue)
Well, this is the main goal that i wanna acheive in this project, to allow user to sing using their own mobile phone. It took me quite some time to learn how to configure, but in the end, the latency is not acceptable for this use case. Anyway, learned something interesting.

## Drawing - fabric.js (worked)
After the mic function failed, I decided to make some other entertainment features to the project. My first idea is to make a barrage like bilibili, but I don't want to just copy. So I came up with this idea of something like a collaborative whiteboard, where users in the room can draw their message on the phone, and then it will be displayed on the big screen. I think this could be fun, especially if someone bring an ipad and draw something beautiful with the apple pencil.

I started with direct manipulation of the HTML 5 canvas, then I find it too tedious, so i found a package call fabric.js, which handle drawing and objects perfectly, and works seamlessly with socketio. 
