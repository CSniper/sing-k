import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      connected: false,
      room_joined: false,
      room_id: null,
      playlist: [],
      jump_in_line: [],
      role: 'remote',
      local_stream: null, 
      room_rtc_connection: null,
      room_rtc_connection_state: '',
  },
  mutations: {
      update_connection_state: function(state, payload){
          state.connected=payload;
      },
      update_room_state: function(state, payload){
          state.room_joined=payload.joined && state.connected;
          state.room_id=payload.room_id;
      },
      update_playlist: function(state, payload){
          state.playlist=payload;
      },
      update_jump_in_line: function(state, payload){
          state.jump_in_line=payload;
      },
      add_icscandidate: function(state, payload){
          state.room_rtc_connection.addIceCandidate(payload);
      },
      mic_off: function(state){
          if(state.local_stream){
              state.local_stream.getTracks().forEach((item) => {item.stop()});
          }
      },
  },
  actions: {
      socket_connect: function(context){
          context.commit('update_connection_state', true);
      },
      socket_room_joined: function(context, payload){
          context.commit('update_room_state', {joined: true, room_id: payload});
      },
      socket_update_playlist: function(context, payload){
          context.commit('update_playlist', payload);
      },
      socket_update_jump_in_line: function(context, payload){
          context.commit('update_jump_in_line', payload);
      },
      socket_room_on_icscandidate: function(context, payload){
          context.commit('add_icscandidate', payload);
      },
  },
  modules: {
  }
})
