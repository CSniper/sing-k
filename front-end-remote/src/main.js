import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSocketIO from 'vue-socket.io';
import { io } from 'socket.io-client';

Vue.config.productionTip = false

Vue.use(new VueSocketIO({
    debug:  ( process.env.NODE_ENV !== 'production' ),
    connection: io(),
    vuex: {
        store,
        actionPrefix: 'socket_',
        mutationPrefix: 'socket_'
    },
}))

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
