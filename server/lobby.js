const socketio = require('socket.io');
const Redis = require('ioredis');
const redis = new Redis(redis_connection_config);
const redisAdapter = require('socket.io-redis');

const room_expire_time = parseInt(process.env.ROOM_EXPIRE_TIME) || 86400;

class lobby {
    constructor(server){
        this.io=socketio(server);
        this.io.adapter(redisAdapter(redis_connection_config));
        this.io.on('connection', (socket) => {
            //Event initiated by Room
            socket.on('room_init', async () => {
                //create new room
                var room_id=null;
                //find unused room_id
                while( ( room_id = Math.random().toString(20).substr(2, 6) ) && ( await redis.get(`room:${room_id}`) ) );
                
                //set up the rest
                this.room_setup_handler(socket, room_id);
            });
            
            socket.on('room_reconnect', async (payload) => {
                if( payload.previous_socket_id == ( await redis.get(`room:${payload.room_id}`) ) ){
                    //rejoin room is condition satisfied
                    this.room_setup_handler(socket, payload.room_id);
                }else{
                    //create new room otherwise
                    var room_id=null;
                    while( ( room_id = Math.random().toString(20).substr(2, 6) ) && ( await redis.get(`room:${room_id}`) ) );
                    this.room_setup_handler(socket, room_id);
                }
            })
            
            //Event initiated by Remote
            socket.on('remote_init', (room_id) => {
                this.remote_init_handler(socket, room_id);
            })
        })
    }
    
    async room_setup_handler(socket, room_id){
        
        //create room structure in redis and join room via socket room
        redis.set(`room:${room_id}`, socket.id);
        redis.set(`playlist:${room_id}`, JSON.stringify([]));
        redis.set(`jump_in_line:${room_id}`, JSON.stringify([]));
        redis.expire(`room:${room_id}`, room_expire_time);
        redis.expire(`playlist:${room_id}`, room_expire_time);
        redis.expire(`jump_in_line:${room_id}`, room_expire_time);
        socket.join(room_id);
        socket.emit('assign_room_id', room_id);
        
        //set events handlers
        socket.on('room_update_playlist', (payload) => {
            this.room_update_playlist_handler(socket, room_id, payload);
        })
        
        socket.on('room_update_jump_in_line', (payload) => {
            this.room_update_jump_in_line_handler(socket, room_id, payload);
        })
        
        socket.on('room_reply_with_answer', (payload) => {
            this.room_reply_with_answer_handler(socket, room_id, payload);
        })
        
        socket.on('room_on_icscandidate', (payload) => {
            this.room_on_icscandidate_handler(socket, room_id, payload);
        })
        
        socket.on('disconnect', () => {
            this.room_disconnect_handler(socket, room_id);
        })
    }
    
    //on room_update_playlist event -> store the playlist then broadcast the playlist to all the remotes
    room_update_playlist_handler(socket, room_id, payload){
        socket.to(room_id).emit('update_playlist', payload);
        redis.set(`playlist:${room_id}`, JSON.stringify(payload));
        redis.expire(`room:${room_id}`, room_expire_time);
        redis.expire(`playlist:${room_id}`, room_expire_time);
        redis.expire(`jump_in_line:${room_id}`, room_expire_time);
    }
    
    room_update_jump_in_line_handler(socket, room_id, payload){
        socket.to(room_id).emit('update_jump_in_line', payload);
        redis.set(`jump_in_line:${room_id}`, JSON.stringify(payload));
        redis.expire(`room:${room_id}`, room_expire_time);
        redis.expire(`playlist:${room_id}`, room_expire_time);
        redis.expire(`jump_in_line:${room_id}`, room_expire_time);
    }
    
    //pass room's reply to specified remote
    room_reply_with_answer_handler(socket, room_id, payload){
        this.io.to(payload.remote_id).emit('room_reply_with_answer', payload.answer);
        redis.expire(`room:${room_id}`, room_expire_time);
        redis.expire(`playlist:${room_id}`, room_expire_time);
        redis.expire(`jump_in_line:${room_id}`, room_expire_time);
    }
    
    //Trickle ICE
    room_on_icscandidate_handler(socket, room_id, payload){
        this.io.to(payload.remote_id).emit('room_on_icscandidate', payload.candidate);
        redis.expire(`room:${room_id}`, room_expire_time);
        redis.expire(`playlist:${room_id}`, room_expire_time);
        redis.expire(`jump_in_line:${room_id}`, room_expire_time);
    }
    
    //on disconnect -> disconnect all remote then delete the room structure.
    room_disconnect_handler(socket, room_id){
        socket.to(room_id).emit('room_closed');
        redis.expire(`room:${room_id}`, 60);
        redis.expire(`playlist:${room_id}`, 60);
        redis.expire(`jump_in_line:${room_id}`, 60);
    }
    
    async remote_init_handler(socket, room_id){
        var room_socket_id;
        if(! ( room_socket_id = await redis.get(`room:${room_id}`) ) ) return; //check if target room exist
        
        //joining the room, push this socket to remote list, update room's remote count and update playlist
        socket.join(room_id);
        socket.emit('room_joined', room_id);
        socket.emit('update_playlist', JSON.parse( await redis.get(`playlist:${room_id}`) ));
        socket.emit('update_jump_in_line', JSON.parse( await redis.get(`jump_in_line:${room_id}`) ));
        
        //set events handlers
        socket.on('remote_add_song_by_id', (payload) => {
            this.remote_add_song_by_id_handler(socket, room_id, payload);
        });
        
        socket.on('remote_skip_current_song', () => {
            this.remote_skip_current_song_handler(socket, room_id);
        })
        
        socket.on('remote_switch_play_pause', () => {
            this.remote_switch_play_pause_handler(socket, room_id);
        })
        
        socket.on('remote_call_in_with_offer', (payload) => {
            this.remote_call_in_with_offer_handler(socket, room_id, payload);
        })
        
        socket.on('remote_on_icscandidate', (payload) => {
            this.remote_on_icscandidate_handler(socket, room_id, payload);
        })
        
        socket.on('remote_shuffle_playlist', () => {
            this.remote_shuffle_playlist_handler(socket, room_id);
        })
        
        socket.on('remote_song_jump_in_line', (payload) => {
            this.remote_song_jump_in_line_handler(socket, room_id, payload);
        })
        
        socket.on('remote_send_drawing', (payload) => {
            this.remote_send_drawing_handler(socket, room_id, payload);
        })
        
        socket.on('disconnect', () => {
            this.remote_disconnect_handler(socket, room_id);
        })
    }
    
    //pass event remote_add_song_by_id to room
    async remote_add_song_by_id_handler(socket, room_id, payload){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_add_song_by_id', payload);
    }
    
    //pass event remote_skip_current_song to room
    async remote_skip_current_song_handler(socket, room_id){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_skip_current_song');
    }
    
    //pass event remote_switch_play_pause to room
    async remote_switch_play_pause_handler(socket, room_id){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_switch_play_pause');
    }
    
    //On event remote call in with offer, pass the event and offer to room with socket id of self
    async remote_call_in_with_offer_handler(socket, room_id, payload){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_call_in_with_offer', {remote_id: socket.id, offer: payload});
    }
    
    //Trickle ICE
    async remote_on_icscandidate_handler(socket, room_id, payload){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_on_icscandidate', {remote_id: socket.id, candidate: payload});
    }
    
    //pass event remote_shuffle_playlist to room
    async remote_shuffle_playlist_handler(socket, room_id){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_shuffle_playlist');
    }
    
    //pass event remote_song_jump_in_line to room
    async remote_song_jump_in_line_handler(socket, room_id, payload){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_song_jump_in_line', payload);
    }
    
    //pass event remote_send_drawing to room
    async remote_send_drawing_handler(socket, room_id, payload){
        this.io.to( await redis.get(`room:${room_id}`) ).emit('remote_send_drawing', payload);
    }
    
    //remove self from room's remote socket list on disconnect
    async remote_disconnect_handler(socket, room_id){
        
    }
}

module.exports = lobby
