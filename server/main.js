//Connection Config to other services
global.redis_connection_config = process.env.REDIS_URL || {
    host: (process.env.REDIS_HOST || '127.0.0.1'),
    port: (process.env.REDIS_PORT || 6379)
}
global.youtube_api_connection_config={
  version: 'v3',
  auth: process.env.YOUTUBE_API_KEY,
}

//modules
const path = require('path');
const express = require('express');
const http = require('http');
const lobby = require('./lobby');

var app = express();
var server = http.createServer(app);
var PORT = process.env.PORT || 3000;
app.use(express.json());

//force https
app.use(function(req, res, next) {
    if ((process.env.NODEJS_FORCE_HTTPS=="true") && (req.get('X-Forwarded-Proto') !== 'https')) {
        res.redirect('https://' + req.get('Host') + req.url);
    } else {
        next();
    }
});

//routing to /room
const room_route = require('./routes/room');
app.use("/room", room_route);

//routing to /youtube
if(process.env.YOUTUBE_API_KEY){
    const youtube_route = require('./routes/youtube');
    app.use("/youtube", youtube_route);
}

app.use( express.static( path.join(__dirname, 'public') ) );

//socket
var k_lobby = new lobby(server);

server.listen(PORT, () => console.log(`server listening on PORT ${PORT}`))
