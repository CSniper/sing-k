const express = require('express');
const path = require('path');
const Redis = require('ioredis');
let router = express.Router();

//YouTube Data Api v3
const {google} = require('googleapis');
const youtube = google.youtube(youtube_api_connection_config);

//redis
const redis = new Redis(redis_connection_config);
const cache_lifetime = parseInt(process.env.CACHE_LIFETIME) || 3600;

router.get('/video/:key/:video_id', async function(req, res) {
    if(!( await redis.get(`room:${req.params.key}`) )) return res.sendStatus(401);
    //try to get from cache first
    var res_data = null;
    try {
        res_data = await redis.hgetall(`video:${req.params.video_id}`);
        if(Object.entries(res_data).length > 0){
            return res.json(res_data);
        }
    } catch (e) {
        console.log(e);
    }
    
    try {
        const query = {
            part: [
                "snippet"
            ],
            id: [
                req.params.video_id
            ]
        };
        const youtube_res = await youtube.videos.list(query);
        if(youtube_res.status==200){
            res_data = {
                id: youtube_res.data.items[0].id,
                title: youtube_res.data.items[0].snippet.title,
                channel: youtube_res.data.items[0].snippet.channelTitle,
                thumbnails: youtube_res.data.items[0].snippet.thumbnails.high.url,
            }
            await redis.hmset(`video:${req.params.video_id}`, res_data);
            await redis.expire(`video:${req.params.video_id}`, cache_lifetime);
            return res.json(res_data);
        }
        return res.sendStatus(404);
    } catch (e) {
        console.log(e);
        return res.sendStatus(404);
    }
});

router.get('/search/:key/:query', async function(req, res) {
    if(!( await redis.get(`room:${req.params.key}`) )) return res.sendStatus(401);
    try {
        const query = {
            part: [
                "snippet"
            ],
            maxResults: 50,
            q: req.params.query,
            type: "video",
            videoEmbeddable: "true",
            videoSyndicated: "true",
        }
        const youtube_res = await youtube.search.list(query);
        if(youtube_res.status==200){
            const res_data = youtube_res.data.items.map( (item) => {
                return {
                    id: item.id.videoId,
                    title: item.snippet.title,
                    channel: item.snippet.channelTitle,
                    thumbnails: item.snippet.thumbnails.high.url
                }
            });
            res_data.forEach( async (item, i) => {
                await redis.hmset(`video:${item.id}`, item);
                await redis.expire(`video:${item.id}`, cache_lifetime);
            });
            return res.json(res_data);
        }
        return res.sendStatus(404);
    } catch (e) {
        console.log(e);
        return res.sendStatus(404);
    }
});

module.exports = router;
