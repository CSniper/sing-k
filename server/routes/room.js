const express = require('express');
const path = require('path');
let router = express.Router();

router.get('/:room_id', function(req, res) {
    res.sendFile(path.join(__dirname + '/../public/room/index.html'));
});

module.exports = router;
