FROM node:16-alpine AS builder
WORKDIR /build
COPY ./common-components/ ./common-components/
COPY ./front-end-room/ ./front-end-room/
COPY ./front-end-remote/ ./front-end-remote/

WORKDIR /build/front-end-room
RUN npm ci && npm run build && mv ./dist /build/front-end

WORKDIR /build/front-end-remote
RUN npm ci && npm run build && mv ./dist /build/front-end/room

FROM node:16-alpine AS Production

# copy source code
WORKDIR /usr/src/app
COPY ./server/ ./server/

WORKDIR /usr/src/app/server
RUN npm ci
COPY --from=builder /build/front-end ./public

EXPOSE 3000
CMD [ "node", "main.js" ]
